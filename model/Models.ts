export interface Entry{
    id:number,
    question: string,
    response: string,
    folderId: number
}
export interface LoginForm{
    email: string,
    password: string
}
export interface RegisterForm{
    email: string,
    password: string
    confirmPassword: string
    firstName: string,
    lastName: string,
}
export interface Folder{
    id:number,
    name: string,
    parentId:number,
    childFolder: Folder[],
}
export interface FolderOptions{
    value:number,
    name:string,
    childFolder:FolderOptions[]

}
export interface FolderForm{
    folderName: string,
    folderParentsId: number,
}
export interface ResponseModel{
   code: number,
   message:string,
   data: object,
}

